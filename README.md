# KayakoServiceAutomation


This project contains the automation code that uses Kayako APIs to execute flows in Kayako.
Please refer to it's wiki for more information.
https://gitlab.com/akashkhot/kayakoserviceautomation/-/wikis/home

Please note that this code is a client code to consume the APIs. 
It is not part of any official release of Kayako product and not supported by Kayako Teams.

You may use this code
 - If it supports your use case. 
 - To modify on your own according to your requirement. 
 - To refer and develop your own code to execute Kayako APIs. 

 No support will be provided for any error or issues in the code. 
