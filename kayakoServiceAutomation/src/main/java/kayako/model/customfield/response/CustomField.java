package kayako.model.customfield.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CustomField {

	String id;
	List<LocaleField> titles;
	String type;
	String key;
	List<LocaleField> descriptions;
	String is_required;
	String is_editable;
	String is_system;
	String regular_expression;
	List<Option> options;

}
