package kayako.model.customfield.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LocaleField {

	String id;
	String locale;
	String translation;
	String created_at;
	String updated_at;
	String resource_type;

}
