package kayako.model.customfield.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Option {

	String id;
	String fielduuid;
	List<LocaleField> values;
	String created_at;
	String updated_at;
	String resource_type;

}
