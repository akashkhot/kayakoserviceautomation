package kayako.model.cases.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CaseRequest {

	private String subject;
	private String contents;
	private String channel;
	private String requesterId;
	private String typeId;

}
