package kayako.model.cases.request;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserInvitationRequest {
	
	private List<UserInvitation> users;

}
