package kayako.model.cases.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserInvitation {
	
	private String fullname;
	private String email;
	private String role_id;
	private String[] team_ids;

}
