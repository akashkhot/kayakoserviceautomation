package kayako.model.cases.response;

import kayako.model.cases.response.activities.Actor;
import kayako.model.generic.response.GenericResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CaseActivities extends GenericResponse {

	private String id;
	private String activity;
	private Actor actor;
	private String verb;
	private String created_at;

}
