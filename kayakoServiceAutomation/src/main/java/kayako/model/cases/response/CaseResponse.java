package kayako.model.cases.response;

import kayako.model.generic.response.GenericResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CaseResponse extends GenericResponse {

	private String id;
	private String subject;
	private String portal;
	private CustomFieldValue[] custom_fields;

}
