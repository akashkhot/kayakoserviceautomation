package kayako.model.cases.response.activities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Actor {
	
	private String name;
	private String title;

}
