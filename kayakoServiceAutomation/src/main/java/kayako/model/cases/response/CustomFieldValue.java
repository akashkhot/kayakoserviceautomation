package kayako.model.cases.response;

import kayako.model.customfield.response.CustomField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CustomFieldValue {

	private CustomField field;
	private String value;
}
