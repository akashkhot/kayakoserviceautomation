package kayako.model.generic.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class Locale {

	private String id;
	private String locale;
	private String name;
	private String native_name;

}