package kayako.model.articles.request;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ArticlesRequest {

	private List<Title> titles;
	private List<Content> contents;
	private String legacyId;
	private String sectionId;
	private String authorId;
	private String keywords;
	private String status;
	private String isFeatured="false";
	private boolean allowComments = true;

}
