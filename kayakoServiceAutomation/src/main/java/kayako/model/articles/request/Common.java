package kayako.model.articles.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Common {

	private String locale;
	private String translation;

}
