package kayako.model.articles.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GetAllSectionsResponse {

	private String id;
	private List<Title> titles;
	private List<Content> contents;

}
