package kayako.model.articles.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GetAllArticlesResponse {

	private String id;
	private List<Title> titles;
	private List<Content> contents;
	private Author author;
	private Section section;
	private String status;
	private String isFeatured;
	private boolean allow_comments;
	private String keywords;
	private List<Attachment> attachments;
	private List<Tag> tags;

}
