package kayako.model.articles.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Brand {

	private String id;
	private String name;

}
