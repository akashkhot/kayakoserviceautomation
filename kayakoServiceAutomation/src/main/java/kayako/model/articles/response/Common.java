package kayako.model.articles.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Common {

	private String id;
	private String locale;
	private String translation;
	private String resourceType;

}
