package kayako.model.org.response;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GetNotesResponse {

	private String id;
	private String body_text;
	private String body_html;
	private String is_pinned;
	private User user;
	private User creator;
	private User pinned_by;
	private String created_at;
	private String updated_at;
	
}
