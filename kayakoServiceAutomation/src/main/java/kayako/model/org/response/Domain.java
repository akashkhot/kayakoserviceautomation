package kayako.model.org.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Domain {
	
	private String id;
	private String domain;

}
