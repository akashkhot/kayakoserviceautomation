package kayako.model.org.response;

import java.util.List;

import kayako.model.generic.response.GenericResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetAllOrgsResponse extends GenericResponse {

	public GetAllOrgsResponse(String id, String name) {
		this.id = id;
		this.name = name;
	}

	private String id;
	private String name;
	private List<Domain> domains;

}
