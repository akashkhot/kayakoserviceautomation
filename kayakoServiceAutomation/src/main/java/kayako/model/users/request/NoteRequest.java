package kayako.model.users.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class NoteRequest {

	private String contents;

	public NoteRequest(String contents) {
		this.contents = contents;
	}

}
