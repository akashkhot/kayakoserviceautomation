package kayako.model.users.response;

import kayako.model.org.response.GetAllOrgsResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserResponse {
	
	private String id;
	private String full_name;
	private UserRole role;
	private GetAllOrgsResponse organization;

}
