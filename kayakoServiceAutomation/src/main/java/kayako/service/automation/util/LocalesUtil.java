package kayako.service.automation.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.generic.response.Locale;

public class LocalesUtil {

	public static Map<String, Locale> getLocales()
			throws UnirestException, FileNotFoundException, IOException, ParseException {

		JSONObject jsonObject = CommonUtil.getJsonObjectFromJsonFile("src/main/resources/locales.json");
		List<Locale> locales = CommonUtil.objectMapper.readValue(jsonObject.get("data").toString(),
				new TypeReference<List<Locale>>() {
				});
		
		Map<String,Locale> localeMap=new HashMap<String,Locale>();
		
		for(Locale locale: locales) {
			localeMap.put(locale.getLocale(), locale);
		}

		return localeMap;
	}

}
