package kayako.service.automation.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.articles.response.GetAllSectionsResponse;

public class SectionUtil {

	public static List<GetAllSectionsResponse> getAllSectionsInCategory(String category)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "api/v1/sections";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();
		params.put("category_ids", category);

		List<GetAllSectionsResponse> sections = new ArrayList<GetAllSectionsResponse>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, "sections for the category: " + category);

		for (String data : dataSet) {
			List<GetAllSectionsResponse> additionalData = CommonUtil.objectMapper.readValue(data,
					new TypeReference<List<GetAllSectionsResponse>>() {
					});

			sections.addAll(additionalData);
		}
		return sections;
	}

	public static List<String> getIdsOfAllSectionsInCategory(String category)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		List<GetAllSectionsResponse> sections = getAllSectionsInCategory(category);

		List<String> sectionIds = new ArrayList<String>();
		for (GetAllSectionsResponse section : sections) {
			sectionIds.add(section.getId());
		}

//		System.out.println("Section Ids retrieved from Category " + category + ":");
//		System.out.println(sectionIds.toString().substring(1, sectionIds.toString().length() - 1));
		return sectionIds;
	}

}
