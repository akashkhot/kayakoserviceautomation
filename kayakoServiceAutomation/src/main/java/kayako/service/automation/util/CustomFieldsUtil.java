package kayako.service.automation.util;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.customfield.response.CustomField;

public class CustomFieldsUtil {

	public static CustomField getCustomField(String customFieldId)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "/api/v1/cases/fields/" + customFieldId;

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();
		params.put("fields", "id,options(values(translation,locale))");

		HttpResponse<JsonNode> response = RequestUtil.buildGetRequest(CommonUtil.domain + apiPath, params).asJson();

		CustomField customField = CommonUtil.objectMapper
				.readValue(response.getBody().getObject().get("data").toString(), new TypeReference<CustomField>() {
				});

		return customField;
	}

}
