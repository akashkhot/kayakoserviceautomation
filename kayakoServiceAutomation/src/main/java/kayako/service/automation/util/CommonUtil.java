package kayako.service.automation.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.generic.response.GenericResponse;

public class CommonUtil {

	protected static String username;
	protected static String password;
	protected static String authToken;
	protected static String authMode;

	public static Properties kayakoProps;
	public static ObjectMapper objectMapper;
	protected static boolean fetchPublicDataOnly;
	protected static Map<String, String> headers;
	public static String domain;

	protected static final String NONE = "NONE";
	protected static final String BASIC = "BASIC";
	protected static final String OAUTH = "OAUTH";
	protected static final int DEFAULT_LIMIT = 200;

	public static void initialize() throws IOException {

		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classloader.getResourceAsStream("kayako.properties");

		kayakoProps = new Properties();
		kayakoProps.load(inputStream);

		username = kayakoProps.getProperty("username");
		password = kayakoProps.getProperty("password");

		domain = kayakoProps.getProperty("domain");

		fetchPublicDataOnly = Boolean.parseBoolean(kayakoProps.getProperty("fetchPublicDataOnly"));

		authMode = kayakoProps.getProperty("authMode");
		authToken = kayakoProps.getProperty("authToken");

		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("content-type", "application/json");
	}

	public static String getCSVFromList(List<String> list) {
		return list.toString().substring(1, list.toString().length() - 1);
	}

	public static Map<String, Object> getBoilerPlateParams() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("include", "*");
		params.put("limit", String.valueOf(DEFAULT_LIMIT));
		return params;
	}

	public static List<String> genericExtractor(String apiPath, Map<String, Object> params, String resourceName)
			throws UnirestException, JsonProcessingException, JsonMappingException {

		System.out.println("------------------------------------------------------------");
		System.out.println("Fetching " + resourceName);

		int currentFetched = 0;
		int totalCount = 0;
		int offset = 0;

		List<GenericResponse> data = new ArrayList<GenericResponse>();

		List<String> dataList = new ArrayList<String>();

		do {

			HttpResponse<JsonNode> response = RequestUtil.buildGetRequest(CommonUtil.domain + apiPath, params).asJson();

			if(response.getStatus()==403) {
				System.out.println("403 response, not allowed to fetch this.");
				continue;
			}
			dataList.add(response.getBody().getObject().get("data").toString());

			List<GenericResponse> additionalData = CommonUtil.objectMapper.readValue(
					response.getBody().getObject().get("data").toString(), new TypeReference<List<GenericResponse>>() {
					});

			data.addAll(additionalData);

			currentFetched = data.size();
			totalCount = Integer.parseInt(response.getBody().getObject().get("total_count").toString());
			int actualLimit = Integer.parseInt(response.getBody().getObject().get("limit").toString());
			offset += actualLimit;
			params.put("offset", String.valueOf(offset));
			params.put("limit", String.valueOf(actualLimit));

			System.out.println("Fetched " + currentFetched + "/" + totalCount + " " + resourceName + " so far");

		} while (totalCount > currentFetched);

		return dataList;

	}

	public static JSONObject getJsonObjectFromJsonFile(String filePath)
			throws IOException, ParseException, FileNotFoundException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(filePath));

		JSONObject jsonObject = (JSONObject) obj;
		return jsonObject;
	}

}
