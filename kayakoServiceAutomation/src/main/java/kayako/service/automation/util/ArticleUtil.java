package kayako.service.automation.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.articles.response.GetAllArticlesResponse;

public class ArticleUtil {

	public static List<GetAllArticlesResponse> getArticlesInSection(String section)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "/api/v1/articles";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();
		params.put("section_id", section);

		List<GetAllArticlesResponse> articles = new ArrayList<GetAllArticlesResponse>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, "articles in the section: "+section);

		for (String data : dataSet) {
			List<GetAllArticlesResponse> additionalData = CommonUtil.objectMapper.readValue(data,
					new TypeReference<List<GetAllArticlesResponse>>() {
					});

			articles.addAll(additionalData);
		}

		return articles;
	}

	public static List<String> getIdsOfAllArticlesInSection(String section)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		List<GetAllArticlesResponse> articles = getArticlesInSection(section);

		List<String> articleIds = new ArrayList<String>();
		for (GetAllArticlesResponse article : articles) {
			articleIds.add(article.getId());
		}

		System.out.println("Article Ids retrieved from Section " + section + ":");
		System.out.println(articleIds.toString().substring(1, articleIds.toString().length() - 1));
		return articleIds;
	}

}
