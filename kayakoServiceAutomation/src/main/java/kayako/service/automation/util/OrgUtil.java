package kayako.service.automation.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.org.response.GetAllOrgsResponse;
import kayako.model.org.response.GetNotesResponse;
import kayako.model.org.response.User;

public class OrgUtil {

	private static final String ORG_SRC_FILE = "REPLACE_FILE_PATH_HERE";

	public static List<GetAllOrgsResponse> getAllOrgs()
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "/api/v1/organizations";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();

		List<GetAllOrgsResponse> orgs = new ArrayList<GetAllOrgsResponse>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, "organizations");

		for (String data : dataSet) {
			List<GetAllOrgsResponse> additionalOrgs = CommonUtil.objectMapper.readValue(data,
					new TypeReference<List<GetAllOrgsResponse>>() {
					});

			orgs.addAll(additionalOrgs);
		}

		return orgs;
	}

	public static List<GetNotesResponse> getNotesForOrg(String orgId)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "/api/v1/organizations/" + orgId + "/notes";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();

		List<GetNotesResponse> notes = new ArrayList<GetNotesResponse>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, "notes for the organization id: " + orgId);

		for (String data : dataSet) {
			List<GetNotesResponse> additionalData = CommonUtil.objectMapper.readValue(data,
					new TypeReference<List<GetNotesResponse>>() {
					});

			notes.addAll(additionalData);
		}

		return notes;
	}

	public static List<GetAllOrgsResponse> getOrgsFromFile() throws NumberFormatException, IOException {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream is = classloader.getResourceAsStream("orgList.txt");

		InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
		BufferedReader reader = new BufferedReader(streamReader);
		List<GetAllOrgsResponse> orgs = new ArrayList<GetAllOrgsResponse>();

		String line = "";
		while ((line = reader.readLine()) != null) {
			String split[] = line.split("~");
			orgs.add(new GetAllOrgsResponse(split[0], split[1]));
		}

		reader.close();
		streamReader.close();
		is.close();
		return orgs;

	}

	public static List<GetAllOrgsResponse> getOrgsFromXLFile() throws NumberFormatException, IOException {
		FileInputStream inputStream = new FileInputStream(new File(ORG_SRC_FILE));

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);

		Iterator<Row> iterator = firstSheet.iterator();
		iterator.next();// skip header row
		List<GetAllOrgsResponse> orgs = new ArrayList<GetAllOrgsResponse>();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			GetAllOrgsResponse org = new GetAllOrgsResponse();
			Cell nextCell = cellIterator.next(); // skip Sr No column
			nextCell = cellIterator.next();
			org.setId(nextCell.getStringCellValue());

			nextCell = cellIterator.next();
			org.setName(nextCell.getStringCellValue());

			orgs.add(org);
		}

		return orgs;

	}

	public static GetAllOrgsResponse getOrg(String id)
			throws JsonMappingException, JsonProcessingException, UnirestException {
		return getOrg(id, true);
	}

	protected static GetAllOrgsResponse getOrg(String id, boolean printLogs)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "/api/v1/organizations/" + id;

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();

		if (printLogs) {
			System.out.println("------------------------------------------------------------");
			System.out.println("Fetching Organization with Id: " + id);
		}

		HttpResponse<JsonNode> response = RequestUtil.buildGetRequest(CommonUtil.domain + apiPath, params).asJson();

		GetAllOrgsResponse org = CommonUtil.objectMapper.readValue(
				response.getBody().getObject().get("data").toString(), new TypeReference<GetAllOrgsResponse>() {
				});

		if (printLogs) {
			System.out.println("Fetched organization");
			printOrgDetails(org);
		}

		return org;
	}

	private static void printOrgDetails(GetAllOrgsResponse org) {
		System.out.println("Id: " + org.getId());
		System.out.println("Name: " + org.getName());
		System.out.print("Domains: " + getCurrentDomains(org));
		System.out.println("\n");

	}

	public static String getCurrentDomains(GetAllOrgsResponse org) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < org.getDomains().size(); i++) {
			sb.append(org.getDomains().get(i++).getDomain());
			if (i != org.getDomains().size()) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	public static List<User> getAllMembersOfOrg(String id)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "/api/v1/organizations/" + id + "/members";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();

		List<User> users = new ArrayList<User>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, "users under organization id: " + id);

		for (String data : dataSet) {
			List<User> additionalData = CommonUtil.objectMapper.readValue(data, new TypeReference<List<User>>() {
			});

			users.addAll(additionalData);
		}

		return users;
	}

	public static void removeDomainsFromOrg(String orgId)
			throws Exception, JsonProcessingException, UnirestException {

		System.out.println("Removing domains from Org with id: " + orgId);

		String apiPath = CommonUtil.domain + "api/v1/organizations/" + orgId;

		HttpResponse<JsonNode> response = RequestUtil.buildPutRequest(apiPath).field("domains", "").asJson();

		if (response.getStatus() == 200) {
			System.out.println("Removed");
		} else {
			System.out.println("Could not remove. API Status: " + response.getStatus());
		}
	}

	public static void addDomainsToOrg(String orgId, String domains)
			throws Exception, JsonProcessingException, UnirestException {

		System.out.println("------------------------------------------------------------");
		System.out.println("Adding below domains from Org with id: " + orgId);
		System.out.println(domains);

		GetAllOrgsResponse org = getOrg(orgId, false);
		String currentDomains = getCurrentDomains(org);
		System.out.println("Current domains in org: " + currentDomains);
		if ("".equals(currentDomains) || currentDomains == null) {
			System.out.print("No Existing domain\n");
		} else {
			domains = currentDomains + ", " + domains;
			System.out.println("New Domain String: " + domains);
		}

		String apiPath = CommonUtil.domain + "api/v1/organizations/" + orgId;

		HttpResponse<JsonNode> response = RequestUtil.buildPutRequest(apiPath).field("domains", domains).asJson();

		if (response.getStatus() == 200) {
			System.out.println("Added");
		} else {
			System.out.println("Could not remove. API Status: " + response.getStatus());
		}
	}

}
