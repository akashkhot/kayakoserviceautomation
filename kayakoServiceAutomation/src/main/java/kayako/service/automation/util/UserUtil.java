package kayako.service.automation.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.cases.request.UserInvitationRequest;
import kayako.model.users.request.NoteRequest;
import kayako.model.users.response.NoteResponse;
import kayako.model.users.response.UserResponse;

public class UserUtil {

	public static void updateUserOrganization(String userId, String orgId)
			throws Exception, JsonProcessingException, UnirestException {
		System.out.println("------------------------------------------------------------");

		System.out.println("Moving user with id: " + userId + " to Org with id: " + orgId);

		String apiPath = CommonUtil.domain + "api/v1/users/" + userId;

		HttpResponse<JsonNode> response = RequestUtil.buildPutRequest(apiPath)
				.field("organization_id", Integer.valueOf(orgId)).asJson();

		if (response.getStatus() == 200) {
			System.out.println("Moved");
		} else {
			System.out.println("Could not move. API Status: " + response.getStatus());
		}
	}

	public static NoteResponse addNotesToUser(String userId, NoteRequest noteRequest)
			throws Exception, JsonProcessingException, UnirestException {
		System.out.println("Adding notes to user: " + userId);

		String apiPath = CommonUtil.domain + "api/v1/users/" + userId + "/notes";

		HttpResponse<JsonNode> response = RequestUtil.buildPostRequest(apiPath)
				.body(CommonUtil.objectMapper.writeValueAsString(noteRequest)).asJson();

		System.out.println("API status: " + response.getStatus());
		NoteResponse noteCreated = null;
		if (response.getStatus() == 201) {

			noteCreated = CommonUtil.objectMapper.readValue(response.getBody().getObject().get("data").toString(),
					new TypeReference<NoteResponse>() {
					});
			System.out.println("Note added : " + noteCreated.getId());

		}
		return noteCreated;
	}

	public static List<UserResponse> getAllUsers() throws Exception {

		String apiPath = "/api/v1/users";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();

		List<UserResponse> users = new ArrayList<UserResponse>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, "users");

		for (String data : dataSet) {
			List<UserResponse> additionalOrgs = CommonUtil.objectMapper.readValue(data,
					new TypeReference<List<UserResponse>>() {
					});

			users.addAll(additionalOrgs);
		}

		return users;
	}

	public static void inviteUsers(UserInvitationRequest userInvitationRequest)
			throws Exception, JsonProcessingException, UnirestException {

		String inviteUsersURL = CommonUtil.domain + "api/v1/users/invite";

		HttpResponse<JsonNode> response = RequestUtil.buildPostRequest(inviteUsersURL)
				.body(CommonUtil.objectMapper.writeValueAsString(userInvitationRequest)).asJson();

		System.out.println("API status: " + response.getStatus());
		
		if(response.getStatus()!=201) {
			System.out.println(response.getBody());
		}

	}

}
