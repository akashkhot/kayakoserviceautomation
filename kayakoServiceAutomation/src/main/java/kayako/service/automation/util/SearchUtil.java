package kayako.service.automation.util;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class SearchUtil {

	public static List<String> searchUsersWithEmail(String email) throws Exception {

		String apiPath = "/api/v1/search?query="+"in%3Ausers%20"+URLEncoder.encode(email, "UTF-8");

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();

//		params.put("query", );
		params.put("include", "user,role");

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, "users");

		return dataSet;
	}

}
