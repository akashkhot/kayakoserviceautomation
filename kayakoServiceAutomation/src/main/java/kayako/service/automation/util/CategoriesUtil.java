package kayako.service.automation.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.articles.response.GetAllCategoriesResponse;

public class CategoriesUtil {

	public static List<GetAllCategoriesResponse> getAllCategoriesInBrand(String brandId)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "api/v1/categories";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();

		String resourceName = "categories";

		if (brandId != null && !brandId.isEmpty()) {
			params.put("brand_id", brandId);
			resourceName = "categories for the brandId: " + brandId;
		}

		List<GetAllCategoriesResponse> categories = new ArrayList<GetAllCategoriesResponse>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, resourceName);

		for (String data : dataSet) {
			List<GetAllCategoriesResponse> additionalData = CommonUtil.objectMapper.readValue(data,
					new TypeReference<List<GetAllCategoriesResponse>>() {
					});

			categories.addAll(additionalData);
		}
		return categories;
	}

	public static List<String> getIdsOfAllCategoriesInBrand(String brandId)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		List<GetAllCategoriesResponse> categories = getAllCategoriesInBrand(brandId);

		List<String> categoryIds = new ArrayList<String>();
		for (GetAllCategoriesResponse category : categories) {
			categoryIds.add(category.getId());
		}

//		System.out.println("Category Ids retrieved from Brand " + brand + ":");
//		System.out.println(categoryIds.toString().substring(1, categoryIds.toString().length() - 1));
		return categoryIds;
	}

}
