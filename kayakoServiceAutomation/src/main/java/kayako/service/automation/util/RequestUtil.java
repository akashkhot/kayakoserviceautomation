package kayako.service.automation.util;

import java.util.HashMap;
import java.util.Map;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;

public class RequestUtil {

	public static HttpRequestWithBody buildDeleteRequest(String url) {
		return (HttpRequestWithBody) addAuthParams(Unirest.delete(url));
	}

	public static HttpRequestWithBody buildPostRequest(String url) {
		return (HttpRequestWithBody) addAuthParams(Unirest.post(url).headers(CommonUtil.headers));
	}

	public static HttpRequestWithBody buildPutRequest(String url) {
		return (HttpRequestWithBody) addAuthParams(Unirest.put(url).headers(preparePutHeaders()));
	}

	private static HttpRequest addAuthParams(HttpRequest request) {
		if (CommonUtil.OAUTH.equals(CommonUtil.authMode)) {
			return request.header("Authorization", CommonUtil.authToken);
		} else if (CommonUtil.BASIC.equals(CommonUtil.authMode)) {
			return request.basicAuth(CommonUtil.username, CommonUtil.password);
		} else {// NONE
			return request;
		}
	}

	private static HashMap<String, String> preparePutHeaders() {
		HashMap<String, String> putHeaders = new HashMap<String, String>();
		putHeaders.put("accept", "application/json");
		putHeaders.put("content-type", "application/x-www-form-urlencoded");
		return putHeaders;
	}

	public static GetRequest buildGetRequest(String url, Map<String, Object> params) {
		GetRequest request = (GetRequest) addAuthParams(Unirest.get(url));

		if (params != null && !params.isEmpty()) {
			request = (GetRequest) request.queryString(params);
		}
		return request;
	}

}
