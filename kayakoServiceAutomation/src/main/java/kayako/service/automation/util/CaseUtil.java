package kayako.service.automation.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.cases.request.CaseRequest;
import kayako.model.cases.response.CaseActivities;
import kayako.model.cases.response.CaseResponse;

public class CaseUtil {

	public static CaseResponse createCase(CaseRequest caseRequest)
			throws Exception, JsonProcessingException, UnirestException {

		String createCaseURL = CommonUtil.domain + "api/v1/cases";
		CaseResponse caseCreated = null;

		HttpResponse<JsonNode> response = RequestUtil.buildPostRequest(createCaseURL)
				.body(CommonUtil.objectMapper.writeValueAsString(caseRequest)).asJson();

		System.out.println("API status: " + response.getStatus());
		if (response.getStatus() == 201) {

			caseCreated = CommonUtil.objectMapper.readValue(response.getBody().getObject().get("data").toString(),
					new TypeReference<CaseResponse>() {
					});

		}
		return caseCreated;

	}

	public static List<CaseResponse> getAllCases()
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "api/v1/cases";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();
//		params.put("fields", "id,custom_fields(field(id),value))");

		String resourceName = "cases";

		List<CaseResponse> cases = new ArrayList<CaseResponse>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, resourceName);

		for (String data : dataSet) {
			List<CaseResponse> additionalData = CommonUtil.objectMapper.readValue(data,
					new TypeReference<List<CaseResponse>>() {
					});

			cases.addAll(additionalData);
		}
		return cases;
	}

	public static List<CaseActivities> getAllActivitiesForACase(String caseId)
			throws UnirestException, JsonProcessingException, JsonMappingException {
		String apiPath = "api/v1/cases/" + caseId + "/activities";

		Map<String, Object> params = CommonUtil.getBoilerPlateParams();

		String resourceName = "Case Activities";

		List<CaseActivities> activities = new ArrayList<CaseActivities>();

		List<String> dataSet = CommonUtil.genericExtractor(apiPath, params, resourceName);

		for (String data : dataSet) {
			List<CaseActivities> additionalData = CommonUtil.objectMapper.readValue(data,
					new TypeReference<List<CaseActivities>>() {
					});

			activities.addAll(additionalData);
		}
		return activities;
	}

}
