package kayako.service.automation.executor;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import kayako.model.cases.request.UserInvitation;
import kayako.model.cases.request.UserInvitationRequest;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.SearchUtil;
import kayako.service.automation.util.UserUtil;

public class InviteUsersInBulk {

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		UserInvitationRequest userInvitation = prepareUserInvitation();
		UserUtil.inviteUsers(userInvitation);
		

	}

	private static UserInvitationRequest prepareUserInvitation() throws Exception {
//		UserInvitation userInvitation = new UserInvitation();
//		userInvitation.setEmail("a.b2@trilogy.com");
//		userInvitation.setFullname("B C");
//		userInvitation.setRole_id("15");
//		String[] team_ids = { "14","96" };
//		userInvitation.setTeam_ids(team_ids);

//		List<UserInvitation> userInvitationList=new ArrayList<UserInvitation>();
//		userInvitationList.add(userInvitation);
		
		UserInvitationRequest userInvitationRequest = new UserInvitationRequest();
		
		
		
		userInvitationRequest.setUsers(getUsersToBeInvitedFromFile());
		return userInvitationRequest;
	}
	
	private static List<UserInvitation> getUsersToBeInvitedFromFile() throws Exception {
		FileInputStream inputStream = new FileInputStream(new File("src/main/resources/vpcsm2.xlsx"));

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);

		Iterator<Row> iterator = firstSheet.iterator();
		iterator.next();// skip header row
		List<UserInvitation> userInvitationList = new ArrayList<UserInvitation>();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			UserInvitation userInvitation = new UserInvitation();
			Cell nextCell = cellIterator.next(); // skip Sr No column
			if(nextCell.getNumericCellValue()<1) {
				break;
			}
			
			nextCell = cellIterator.next();
			userInvitation.setFullname(nextCell.getStringCellValue());

			nextCell = cellIterator.next();
			userInvitation.setEmail(nextCell.getStringCellValue());

//			List<String> dataSet = SearchUtil.searchUsersWithEmail(nextCell.getStringCellValue());
//			
//			if(dataSet.size()>0) {
//				System.out.println("Found "+ dataSet.size()+" matches for "+nextCell.getStringCellValue());
//				continue;
//			}
			
			nextCell = cellIterator.next();
//			userInvitation.setRole_id(String.valueOf(nextCell.getNumericCellValue()));
			userInvitation.setRole_id(nextCell.getStringCellValue());
			
			nextCell = cellIterator.next();
			userInvitation.setTeam_ids(nextCell.getStringCellValue().split(","));
//			userInvitation.setTeam_ids(String.valueOf(nextCell.getNumericCellValue()));

			userInvitationList.add(userInvitation);
		}

		return userInvitationList;

	}

}
