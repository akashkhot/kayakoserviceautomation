package kayako.service.automation.executor;

import kayako.model.users.request.NoteRequest;
import kayako.model.users.response.NoteResponse;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.UserUtil;

public class BulkUserNotesCreator {

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		NoteRequest noteRequest = new NoteRequest("Note ");

		String notesForUserId = CommonUtil.kayakoProps.getProperty("notesForUserId");
		int numberOfNotes = Integer.parseInt(CommonUtil.kayakoProps.getProperty("numberOfNotes"));

		int count = 0;

		for (int i = 1; i <= numberOfNotes; i++) {
			noteRequest.setContents("Test Note " + i);
			NoteResponse noteResponse = UserUtil.addNotesToUser(notesForUserId, noteRequest);

			if (noteResponse != null) {
				count++;
				System.out.println("Current count is: " + count);
			}
		}

		System.out.println("-----------------------------------------\n\n");
		System.out.println("Total number of cases created: " + count);

	}

}
