package kayako.service.automation.executor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.org.response.GetAllOrgsResponse;
import kayako.model.org.response.GetNotesResponse;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.OrgUtil;

public class OrgNotesExtractor {

	private static XSSFWorkbook workbook;
	private static XSSFSheet spreadsheet;
	private static int rowid;

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		initiateXLFile();

		List<GetAllOrgsResponse> orgs = OrgUtil.getOrgsFromXLFile();

		fetchNotes(orgs);

		publishTheFile();

	}

	private static void fetchNotes(List<GetAllOrgsResponse> orgs)
			throws UnirestException, JsonProcessingException, JsonMappingException, InterruptedException {
		int calls = 0;

		for (GetAllOrgsResponse org : orgs) {

			List<GetNotesResponse> notes = OrgUtil.getNotesForOrg(org.getId());

			if (notes == null || notes.isEmpty()) {
				addNoNotesRow(org);
			} else {

				for (GetNotesResponse note : notes) {
					addARowForNote(org, note);
				}
			}

			calls++;
			if (calls == 290) {
				System.out.println("290 calls done, waiting for 40 seconds");
				calls = 0;
				TimeUnit.SECONDS.sleep(40);
			}
		}
	}

	private static void addNoNotesRow(GetAllOrgsResponse org) {

		XSSFRow row = spreadsheet.createRow(rowid);
		int cellid = 0;

		Cell cell = row.createCell(cellid++);
		cell.setCellValue(rowid++);

		cell = row.createCell(cellid++);
		cell.setCellValue(org.getId());

		cell = row.createCell(cellid++);
		cell.setCellValue(org.getName());

		cell = row.createCell(cellid++);
		cell.setCellValue("No notes found");

	}

	private static void publishTheFile() throws FileNotFoundException, IOException {
		
		String fileName = "src/main/resources/notes_" + System.currentTimeMillis() + ".xlsx";
		FileOutputStream out = new FileOutputStream(new File(fileName));
		System.out.println("------------------------------------------------------------");
		System.out.println("Published " + fileName);
		System.out.println("Done");

		workbook.write(out);
		out.close();
	}

	private static void initiateXLFile() {
		workbook = new XSSFWorkbook();
		spreadsheet = workbook.createSheet("Notes");
		rowid = 0;

		createHeaderRow();
	}

	private static void printNotesRecord(GetNotesResponse note) {
		StringBuilder sb = new StringBuilder();
		sb.append(note.getId()).append('|');
		sb.append(note.getIs_pinned()).append('|');
		sb.append(note.getUser().getFull_name()).append('|');
		sb.append(note.getCreator().getFull_name()).append('|');
		if (note.getPinned_by() != null) {
			sb.append(note.getPinned_by().getFull_name()).append('|');
		} else {
			sb.append('|');
		}
		sb.append(note.getCreated_at()).append('|');
		sb.append(note.getUpdated_at()).append('|');
		sb.append(note.getBody_html()).append('|');
		System.out.println(sb.toString());
	}

	private static void addARowForNote(GetAllOrgsResponse org, GetNotesResponse note) {

		XSSFRow row = spreadsheet.createRow(rowid);
		int cellid = 0;

		Cell cell = row.createCell(cellid++);
		cell.setCellValue(rowid++);

		cell = row.createCell(cellid++);
		cell.setCellValue(org.getId());

		cell = row.createCell(cellid++);
		cell.setCellValue(org.getName());

		cell = row.createCell(cellid++);
		cell.setCellValue(note.getId());

		cell = row.createCell(cellid++);
		cell.setCellValue(note.getCreator().getFull_name());

		cell = row.createCell(cellid++);
		cell.setCellValue(note.getCreated_at());

		cell = row.createCell(cellid++);
		cell.setCellValue(note.getUser().getFull_name());

		cell = row.createCell(cellid++);
		cell.setCellValue(note.getUpdated_at());

		cell = row.createCell(cellid++);
		cell.setCellValue(note.getIs_pinned());

		cell = row.createCell(cellid++);
		if (note.getPinned_by() != null) {
			cell.setCellValue(note.getPinned_by().getFull_name());
		} else {
			cell.setCellValue("");
		}

		cell = row.createCell(cellid++);
		cell.setCellValue(note.getBody_text());

		cell = row.createCell(cellid++);
		cell.setCellValue(note.getBody_html());

	}

	private static void createHeaderRow() {

		XSSFRow row = spreadsheet.createRow(rowid++);
		int cellid = 0;

		Cell cell = row.createCell(cellid++);
		cell.setCellValue("Sr. No.");

		cell = row.createCell(cellid++);
		cell.setCellValue("Organization Id");

		cell = row.createCell(cellid++);
		cell.setCellValue("Organization Name");

		cell = row.createCell(cellid++);
		cell.setCellValue("Note Id");

		cell = row.createCell(cellid++);
		cell.setCellValue("Created by");

		cell = row.createCell(cellid++);
		cell.setCellValue("Created At");

		cell = row.createCell(cellid++);
		cell.setCellValue("Updated by");

		cell = row.createCell(cellid++);
		cell.setCellValue("Updated At");

		cell = row.createCell(cellid++);
		cell.setCellValue("Is Pinned");

		cell = row.createCell(cellid++);
		cell.setCellValue("Pinned by");

		cell = row.createCell(cellid++);
		cell.setCellValue("Body Text");

		cell = row.createCell(cellid++);
		cell.setCellValue("Body HTML");
	}

}
