package kayako.service.automation.executor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import kayako.model.org.response.GetAllOrgsResponse;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.OrgUtil;

public class OrgLister {

	private static XSSFWorkbook workbook;
	private static XSSFSheet spreadsheet;
	private static int rowid;

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		initiateXLFile();

		List<GetAllOrgsResponse> orgs = OrgUtil.getAllOrgs();

		for (GetAllOrgsResponse org : orgs) {
			addOrgRow(org);
		}

		publishTheFile();
		System.out.println("Done");

	}

	private static void publishTheFile() throws IOException {
		String fileName = "src/main/resources/orgs_" + System.currentTimeMillis() + ".xlsx";
		FileOutputStream out = new FileOutputStream(new File(fileName));

		workbook.write(out);
		System.out.println("------------------------------------------------------------");
		System.out.println("Published " + fileName);
		out.close();
	}

	private static void printOrgRecords(List<GetAllOrgsResponse> orgs) {
		System.out.println("Org Id, Org Name");
		for (GetAllOrgsResponse org : orgs) {
			StringBuilder sb = new StringBuilder();
			sb.append(org.getId()).append('~');
			sb.append(org.getName()).append('~');
			System.out.println(sb.toString());
		}
	}

	private static void addOrgRow(GetAllOrgsResponse org) {

		XSSFRow row = spreadsheet.createRow(rowid);
		int cellid = 0;

		Cell cell = row.createCell(cellid++);
		cell.setCellValue(rowid++);

		cell = row.createCell(cellid++);
		cell.setCellValue(org.getId());

		cell = row.createCell(cellid++);
		cell.setCellValue(org.getName());

	}

	private static void initiateXLFile() {
		workbook = new XSSFWorkbook();
		spreadsheet = workbook.createSheet("Organizations");
		rowid = 0;

		createHeaderRow();
	}

	private static void createHeaderRow() {

		XSSFRow row = spreadsheet.createRow(rowid++);
		int cellid = 0;

		Cell cell = row.createCell(cellid++);
		cell.setCellValue("Sr. No.");

		cell = row.createCell(cellid++);
		cell.setCellValue("Organization Id");

		cell = row.createCell(cellid++);
		cell.setCellValue("Organization Name");
	}

}
