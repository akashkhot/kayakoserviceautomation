package kayako.service.automation.executor;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.org.response.GetAllOrgsResponse;
import kayako.model.org.response.User;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.OrgUtil;
import kayako.service.automation.util.UserUtil;

public class OrgMerger {

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		mergeOrgs();

	}

	private static void mergeOrgs() throws UnirestException, JsonProcessingException, JsonMappingException, Exception {
		String sourceOrgs = CommonUtil.kayakoProps.getProperty("sourceOrgs");
		System.out.println("------------------------------------------------------------");
		System.out.println("Source org ids: " + sourceOrgs);
		System.out.println("All users and domains from these will be moved to the target org.");
		String targetOrg = CommonUtil.kayakoProps.getProperty("targetOrg");
		System.out.println("Target Org id: " + targetOrg);

		String[] split = sourceOrgs.split(", ");

		for (String orgId : split) {
			System.out.println(
					"------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Current Org Id is: " + orgId);

			GetAllOrgsResponse org = OrgUtil.getOrg(orgId);

			List<User> users = OrgUtil.getAllMembersOfOrg(orgId);

			for (User user : users) {
				UserUtil.updateUserOrganization(user.getId(), targetOrg);
			}

			System.out.println("------------------------------------------------------------");
			System.out.println("Moved all users from org id: " + orgId);
			System.out.println("------------------------------------------------------------");

			OrgUtil.removeDomainsFromOrg(orgId);
			OrgUtil.addDomainsToOrg(targetOrg, OrgUtil.getCurrentDomains(org));

		}

		System.out.println(
				"------------------------------------------------------------------------------------------------------------------------");

		System.out.println("Merge complete");
	}

}
