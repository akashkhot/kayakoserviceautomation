package kayako.service.automation.executor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import kayako.model.customfield.response.CustomField;
import kayako.model.customfield.response.LocaleField;
import kayako.model.customfield.response.Option;
import kayako.model.generic.response.Locale;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.CustomFieldsUtil;
import kayako.service.automation.util.LocalesUtil;

public class CustomFieldPrinter {

	private static XSSFWorkbook workbook;
	private static XSSFSheet spreadsheet;
	private static int rowid;

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		CustomField cf = CustomFieldsUtil.getCustomField(CommonUtil.kayakoProps.getProperty("customFieldId"));

		List<Map<String, String>> rows = new ArrayList<Map<String, String>>();

		List<String> header = new ArrayList<String>();
		header.add("No.");
		header.add("Option Id");
		int rownum = 1;

		for (Option option : cf.getOptions()) {
			Map<String, String> row = new HashMap<String, String>();
			row.put("No.", String.valueOf(rownum));
			row.put("Option Id", option.getId());
			for (LocaleField value : option.getValues()) {
				if (rownum == 1) {
					header.add(value.getLocale());
				}
				row.put(value.getLocale(), value.getTranslation());
			}
			rows.add(row);
			rownum++;
		}

		initiateXLFile(rows, header);
		addValueRows(rows, header);

		publishTheFile();
		System.out.println("Done");

	}

	private static void publishTheFile() throws IOException {
		String fileName = "src/main/resources/cfValues_" + System.currentTimeMillis() + ".xlsx";
		FileOutputStream out = new FileOutputStream(new File(fileName));

		workbook.write(out);
		System.out.println("------------------------------------------------------------");
		System.out.println("Published " + fileName);
		out.close();
	}

	private static void addValueRows(List<Map<String, String>> rows, List<String> header) {

		for (Map<String, String> row : rows) {
			XSSFRow sheetRow = spreadsheet.createRow(rowid++);
			int cellid = 0;

			for (int i = 0; i < header.size(); i++) {
				Cell cell = sheetRow.createCell(cellid++);
				cell.setCellValue(row.get(header.get(i)));
			}
		}
	}

	private static void initiateXLFile(List<Map<String, String>> rows, List<String> header) throws Exception {
		workbook = new XSSFWorkbook();
		spreadsheet = workbook.createSheet("Custom Field Values");
		rowid = 0;

		createHeaderRow(header);
	}

	private static void createHeaderRow(List<String> header) throws Exception {

		XSSFRow row = spreadsheet.createRow(rowid++);
		int cellid = 0;

		Cell cell = row.createCell(cellid++);
		cell.setCellValue(header.get(0));

		cell = row.createCell(cellid++);
		cell.setCellValue(header.get(1));

		for (int i = 2; i < header.size(); i++) {
			cell = row.createCell(cellid++);
			cell.setCellValue(prepareLocaleName(LocalesUtil.getLocales(), header.get(i)));
		}

	}

	private static String prepareLocaleName(Map<String, Locale> locales, String locale) {
		if (locales.get(locale) != null) {
			return locales.get(locale).getNative_name() + " (" + locale + ")";
		} else {
			return locale;
		}
	}

}
