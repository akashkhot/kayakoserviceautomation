package kayako.service.automation.executor;

import kayako.model.cases.request.CaseRequest;
import kayako.model.cases.response.CaseResponse;
import kayako.service.automation.util.CaseUtil;
import kayako.service.automation.util.CommonUtil;

public class BulkCaseCreator {

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();
		
		int logicalNumberStart=Integer.parseInt(CommonUtil.kayakoProps.getProperty("logicalNumberStart"));
		int totalCasesToBeCreated=Integer.parseInt(CommonUtil.kayakoProps.getProperty("totalCasesToBeCreated"));

		CaseRequest caseRequest = prepareCaseRequest();

		int count = 0;

		for (int i = logicalNumberStart; i < (logicalNumberStart+totalCasesToBeCreated); i++) {
			caseRequest.setSubject("Test Subject " + i);
			CaseResponse caseResponse = CaseUtil.createCase(caseRequest);

			if (caseResponse != null) {
				count++;
				System.out.println("Case created, id is: " + caseResponse.getId());
				System.out.println("Current count is: " + count);
			}
		}

		System.out.println("-----------------------------------------\n\n");
		System.out.println("Total number of cases created: " + count);

	}

	private static CaseRequest prepareCaseRequest() {
		CaseRequest caseRequest = new CaseRequest();

		caseRequest.setContents("The very first note");
		caseRequest.setChannel("NOTE");
		caseRequest.setRequesterId(CommonUtil.kayakoProps.getProperty("requesterId"));
		caseRequest.setTypeId("1");
		return caseRequest;
	}

}
