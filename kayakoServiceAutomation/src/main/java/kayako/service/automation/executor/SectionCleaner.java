package kayako.service.automation.executor;

import java.util.List;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.service.automation.util.ArticleUtil;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.RequestUtil;

public class SectionCleaner {

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		List<String> articleIds = ArticleUtil
				.getIdsOfAllArticlesInSection(CommonUtil.kayakoProps.getProperty("sectionToBeEmptied"));

		if (articleIds.size() == 0) {
			System.out.println("Nothing to delete");
			return;
		}
		deleteAllArticles(articleIds);
	}

	private static void deleteAllArticles(List<String> articleIds) throws UnirestException {
		System.out.println("----------------------------------------------------");
		System.out.println("Deleting All Articles in section at once.");

		HttpResponse<JsonNode> response = RequestUtil.buildDeleteRequest(CommonUtil.domain + "api/v1/articles")
				.queryString("ids", CommonUtil.getCSVFromList(articleIds)).asJson();

		System.out.println("API call status: " + response.getStatus());
	}

}
