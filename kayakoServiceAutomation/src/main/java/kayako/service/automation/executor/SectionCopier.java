package kayako.service.automation.executor;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;

import kayako.model.articles.request.ArticlesRequest;
import kayako.model.articles.request.Content;
import kayako.model.articles.request.Title;
import kayako.model.articles.response.Attachment;
import kayako.model.articles.response.GetAllArticlesResponse;
import kayako.service.automation.util.ArticleUtil;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.RequestUtil;

public class SectionCopier {

	private static String sourceSection;
	private static String targetSection;
	private static String targetStatus;
	private static boolean copyAllowComments;
	private static boolean copyFeaturedFlag;
	private static boolean copyKeywords;

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		initialize();

		List<GetAllArticlesResponse> articles = ArticleUtil.getArticlesInSection(sourceSection);

		createArticlesInTargetSection(articles);

	}

	private static void createArticlesInTargetSection(List<GetAllArticlesResponse> articles)
			throws Exception, JsonProcessingException, UnirestException {

		printInitialLogs(articles);

		String createArticleURL = CommonUtil.domain + "api/v1/articles";

		for (GetAllArticlesResponse articleResponse : articles) {

			System.out.println("Copying Article with Id: " + articleResponse.getId());

			HttpResponse<JsonNode> response = RequestUtil.buildPostRequest(createArticleURL)
					.body(prepareArticleRequestJson(articleResponse)).asJson();

			System.out.println("API status: " + response.getStatus());

		}
		System.out.println("------------------------------------------------\n");
		System.out.println("Process completed");
	}

	private static String prepareArticleRequestJson(GetAllArticlesResponse articleResponse)
			throws Exception, JsonProcessingException {
		ArticlesRequest articleRequest = new ArticlesRequest();

		articleRequest.setTitles(prepareTitles(articleResponse.getTitles()));
		articleRequest.setContents(prepareContents(articleResponse.getContents()));

		articleRequest.setSectionId(targetSection);
		articleRequest.setAuthorId(articleResponse.getAuthor().getId());

		if (copyKeywords) {
			articleRequest.setKeywords(articleResponse.getKeywords());
		}

		articleRequest.setStatus("SAME".equals(targetStatus) ? articleResponse.getStatus() : targetStatus);

		if (copyFeaturedFlag) {
			articleRequest.setIsFeatured(articleResponse.getIsFeatured());
		}

		if (copyAllowComments) {
			articleRequest.setAllowComments(articleResponse.isAllow_comments());
		}

		return CommonUtil.objectMapper.writeValueAsString(articleRequest);
	}

	private static String getFileIds(GetAllArticlesResponse articleResponse) {
		List<Attachment> files = articleResponse.getAttachments();
		if (files != null && !files.isEmpty()) {
			List<String> fileIds = new ArrayList<String>();
			for (Attachment file : files) {
				fileIds.add(file.getId());
			}
			return CommonUtil.getCSVFromList(fileIds);
		}
		return null;

	}

	private static void printInitialLogs(List<GetAllArticlesResponse> articles) {
		System.out.println("Domain: " + CommonUtil.domain);
		System.out.println("Source Section: " + sourceSection);

		System.out.println("Retrieved " + articles.size() + " from the section");

		System.out.println("Target Section: " + targetSection);

		System.out.println("\nStarting to copy articles.");
		System.out.println("------------------------------------------------");
	}

	private static List<Content> prepareContents(List<kayako.model.articles.response.Content> contents)
			throws Exception {
		List<Content> targetContents = new ArrayList<Content>();
		for (kayako.model.articles.response.Content content : contents) {
			Content targetContent = CommonUtil.objectMapper.convertValue(content, Content.class);

			targetContents.add(targetContent);

		}
		return targetContents;
	}

	private static List<Title> prepareTitles(List<kayako.model.articles.response.Title> titles) throws Exception {
		List<Title> targetTitles = new ArrayList<Title>();
		for (kayako.model.articles.response.Title title : titles) {

			Title targetTitle = CommonUtil.objectMapper.convertValue(title, Title.class);

			targetTitles.add(targetTitle);

		}
		return targetTitles;
	}

	private static void initialize() throws Exception {
		sourceSection = CommonUtil.kayakoProps.getProperty("sourceSection");
		targetSection = CommonUtil.kayakoProps.getProperty("targetSection");
		targetStatus = CommonUtil.kayakoProps.getProperty("targetStatus");
		copyAllowComments = Boolean.valueOf(CommonUtil.kayakoProps.getProperty("copyAllowComments"));
		copyFeaturedFlag = Boolean.valueOf(CommonUtil.kayakoProps.getProperty("copyFeaturedFlag"));
		copyKeywords = Boolean.valueOf(CommonUtil.kayakoProps.getProperty("copyKeywords"));
	}

}
