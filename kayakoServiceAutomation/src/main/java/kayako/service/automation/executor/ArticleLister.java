package kayako.service.automation.executor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import kayako.model.articles.response.GetAllArticlesResponse;
import kayako.model.articles.response.GetAllCategoriesResponse;
import kayako.model.articles.response.GetAllSectionsResponse;
import kayako.service.automation.util.ArticleUtil;
import kayako.service.automation.util.CategoriesUtil;
import kayako.service.automation.util.CommonUtil;
import kayako.service.automation.util.SectionUtil;

public class ArticleLister {

	private static XSSFWorkbook workbook;
	private static XSSFSheet spreadsheet;
	private static int rowid;

	public static void main(String[] args) throws Exception {
		CommonUtil.initialize();

		initiateXLFile();

		List<GetAllCategoriesResponse> categories = CategoriesUtil
				.getAllCategoriesInBrand(CommonUtil.kayakoProps.getProperty("brandId"));

		for (GetAllCategoriesResponse category : categories) {
			List<GetAllSectionsResponse> sections = SectionUtil.getAllSectionsInCategory(category.getId());

			for (GetAllSectionsResponse section : sections) {
				List<GetAllArticlesResponse> articles = ArticleUtil.getArticlesInSection(section.getId());
				for (GetAllArticlesResponse article : articles) {
					addArticleRow(category, section, article);

				}
			}
		}

		publishTheFile();
		System.out.println("Done");

	}

	private static void addArticleRow(GetAllCategoriesResponse category, GetAllSectionsResponse section,
			GetAllArticlesResponse article) {

		XSSFRow row = spreadsheet.createRow(rowid);
		int cellid = 0;

		Cell cell = row.createCell(cellid++);
		cell.setCellValue(rowid++);

		cell = row.createCell(cellid++);
		if (category.getBrand() != null) {
			cell.setCellValue(category.getBrand().getId());
		} else {
			cell.setCellValue("");
		}

		cell = row.createCell(cellid++);
		if (category.getBrand() != null) {
			cell.setCellValue(category.getBrand().getName());
		} else {
			cell.setCellValue("");
		}

		cell = row.createCell(cellid++);
		cell.setCellValue(category.getId());

		cell = row.createCell(cellid++);
		cell.setCellValue(category.getTitles().get(0).getTranslation());

		cell = row.createCell(cellid++);
		cell.setCellValue(section.getId());

		cell = row.createCell(cellid++);
		cell.setCellValue(section.getTitles().get(0).getTranslation());

		cell = row.createCell(cellid++);
		cell.setCellValue(article.getId());

		cell = row.createCell(cellid++);
		cell.setCellValue(article.getTitles().get(0).getTranslation());

	}

	private static void printArticleRecord(GetAllCategoriesResponse category, GetAllSectionsResponse section,
			GetAllArticlesResponse article) {
		StringBuilder sb = new StringBuilder();
		sb.append(category.getBrand().getId()).append(',');
		sb.append(category.getBrand().getName()).append(',');
		sb.append(category.getId()).append(',');
		sb.append(category.getTitles().get(0).getTranslation()).append(',');
		sb.append(section.getId()).append(',');
		sb.append(section.getTitles().get(0).getTranslation()).append(',');
		sb.append(article.getId()).append(',');
		sb.append(article.getTitles().get(0).getTranslation());
		System.out.println(sb.toString());
	}

	private static void initiateXLFile() {
		workbook = new XSSFWorkbook();
		spreadsheet = workbook.createSheet("Articles");
		rowid = 0;

		createHeaderRow();
	}

	private static void publishTheFile() throws IOException {
		String fileName = "src/main/resources/articles_" + System.currentTimeMillis() + ".xlsx";
		FileOutputStream out = new FileOutputStream(new File(fileName));

		workbook.write(out);
		System.out.println("------------------------------------------------------------");
		System.out.println("Published " + fileName);
		out.close();
	}

	private static void createHeaderRow() {

		XSSFRow row = spreadsheet.createRow(rowid++);
		int cellid = 0;

		Cell cell = row.createCell(cellid++);
		cell.setCellValue("Sr. No.");

		cell = row.createCell(cellid++);
		cell.setCellValue("Brand Id");

		cell = row.createCell(cellid++);
		cell.setCellValue("Brand Name");

		cell = row.createCell(cellid++);
		cell.setCellValue("Category Id");

		cell = row.createCell(cellid++);
		cell.setCellValue("Category");

		cell = row.createCell(cellid++);
		cell.setCellValue("Section Id");

		cell = row.createCell(cellid++);
		cell.setCellValue("Section");

		cell = row.createCell(cellid++);
		cell.setCellValue("Article Id");

		cell = row.createCell(cellid++);
		cell.setCellValue("Article Name");
	}

}
